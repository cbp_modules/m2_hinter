# README #

Hinter Module for Magento 2.

### What is this repository for? ###

Hinter Module allows you to see additional block info in Magento's Layout.

### Installation Instructions ###


1. Make sure git and composer are both installed on server.
2. Switch to Magento root folder
3. ``` composer config repositories.leonidas_ict-m2_hinter git https://lpalaiokostas@bitbucket.org/cbp_modules/m2_hinter.git ```
4. ``` composer require leonidas/hinter:dev-master ```
5. ``` php bin/magento module:enable Leonidas_Hinter --clear-static-content ```
6. ``` php bin/magento cache:clean ```
7. ``` php bin/magento setup:upgrade ```