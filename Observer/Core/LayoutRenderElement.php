<?php 


namespace Leonidas\Hinter\Observer\Core;
 
 
class LayoutRenderElement implements \Magento\Framework\Event\ObserverInterface {

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * Hinter config path
     */
    const XML_PATH_HINTER_STATUS = 'hinter/options/enabled';
    const XML_PATH_HINTER_CLICK  = 'hinter/options/ctrl_click';
    const XML_PATH_HINTER_PARAM  = 'hinter/options/use_param';

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Request\Http $request
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->request = $request;
    }
	
	public function execute(
		\Magento\Framework\Event\Observer $observer
	)
    {
        if ((boolean)$this->scopeConfig->getValue(self::XML_PATH_HINTER_STATUS, \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
            if ((boolean) !$this->scopeConfig->getValue(self::XML_PATH_HINTER_PARAM, \Magento\Store\Model\ScopeInterface::SCOPE_STORE) ||
                (
                    (boolean)$this->scopeConfig->getValue(self::XML_PATH_HINTER_PARAM, \Magento\Store\Model\ScopeInterface::SCOPE_STORE)
                    && $this->request->getParam('hinter') == 1
                )
            ) {
                $normalOutput = $observer->getTransport()->getData('output');
                $element_name = $observer->getData('element_name');
                $block = $observer->getLayout()->getBlock($element_name);

                if ($block instanceof \Magento\Framework\View\Element\AbstractBlock) {
                    $template = $block->getTemplateFile();
                    $class = $block->getType();

                    $template = strlen(trim($template)) > 0 ? $template : 'N/A';
                    $class = strlen(trim($class)) > 0 ? $class : 'N/A';
                } else
                    $template = $class = 'N/A';

                $element_name = strlen(trim($element_name)) > 0 ? $element_name : 'N/A';

                $normalOutput = preg_replace(
                    '/(?<=[^\']\<)(\w+?)([ \>])(?!block-template)/',
                    '$1 block-template=' . $template . ' block-element_name=' . $element_name . ' block-class=' . $class . ' block-hinter=true $2',
                    $normalOutput
                );

                if ($element_name == 'footer' && (boolean)$this->scopeConfig->getValue(self::XML_PATH_HINTER_CLICK, \Magento\Store\Model\ScopeInterface::SCOPE_STORE))
                    $normalOutput .= "<style>.cvghinter{ background-color: rgba(240,128,128,1)!important }</style><script>window.onload=function(){var a=!1;jQuery(document).on(\"keyup keydown\",function(b){a=b.ctrlKey,console.log(a?\"ON\":\"OFF\")}),jQuery(document).on(\"click\",'[block-hinter=\"true\"]',function(b){if(a){a=!1;var c=jQuery(this),d=jQuery('[block-template=\"'+c.attr(\"block-template\")+'\"]');b.preventDefault(),b.stopPropagation(),d.addClass(\"cvghinter\"),setTimeout(function(){d.removeClass(\"cvghinter\")},2e3),alert(c.attr(\"block-template\")),console.log(c.attr(\"block-template\")),console.warn(c.attr(\"block-class\")),console.error(c.attr(\"block-element_name\"))}})};</script>";

                $observer->getTransport()->setData('output', $normalOutput);
            }
        }
    }
}
